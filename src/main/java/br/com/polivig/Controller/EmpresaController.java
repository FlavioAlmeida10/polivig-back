package br.com.polivig.Controller;

import br.com.polivig.DTOs.EmpresaDTO;
import br.com.polivig.Entity.CompanyEntity;
import br.com.polivig.Service.CompanyService;
import br.com.polivig.Service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/open")
public class EmpresaController {

    @Autowired
    private EmpresaService empresaService;

    @Autowired
    private CompanyService companyService;

    public EmpresaController(EmpresaService empresaService, CompanyService companyService) {
        this.empresaService = empresaService;
        this.companyService = companyService;
    }

    @GetMapping("/empresa/{cnpj}")
    public ResponseEntity<EmpresaDTO> consultaCNPJ(@PathVariable String cnpj) {


        return empresaService.searchEmpresa(cnpj);
    }

    @PostMapping("/empresa")
    private ResponseEntity<CompanyEntity> cadastroEmpresa(@RequestBody EmpresaDTO empresaDTO){
        ResponseEntity<CompanyEntity> company = companyService.save(empresaDTO);
        return company;
    }

    @GetMapping("/empresa/cad/{cnpj}")
    public EmpresaDTO consultaLocalCNPJ(@PathVariable String cnpj) {
        return companyService.searchEmpresa(cnpj);
    }


}
