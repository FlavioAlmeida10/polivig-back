package br.com.polivig.Controller;

import br.com.polivig.Entity.UserEntity;
import br.com.polivig.Repository.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/user"})
public class UserController {

    private UserRepository userRepo;

    public UserController(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @GetMapping
    public List findAll(){
        return userRepo.findAll();
    }

    @GetMapping(path = {"/{id}"})
    public ResponseEntity findById(@PathVariable long id){
        return userRepo.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public UserEntity create(@RequestBody UserEntity user){
        System.out.println("User id: " + user);
        return userRepo.save(user);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity update(@PathVariable("id") long id,
                                 @RequestBody UserEntity user) {
        return userRepo.findById(id)
                .map(record -> {
                    record.setUsername(user.getUsername());
                    record.setEmail(user.getEmail());
                    record.setPhone(user.getPhone());
                    UserEntity updated = userRepo.save(record);
                    return ResponseEntity.ok().body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping(path ={"/{id}"})
    public ResponseEntity <?> delete(@PathVariable long id) {
        return userRepo.findById(id)
                .map(record -> {
                    userRepo.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }
}
