package br.com.polivig.Controller;

import br.com.polivig.DTOs.EstadoDTO;
import br.com.polivig.Entity.StateEntity;
import br.com.polivig.Service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/open")
public class EstadoController {

    @Autowired
    private EstadoService estadoService;

    public EstadoController(EstadoService estadoService) {
        this.estadoService = estadoService;
    }

    @PostMapping(path = "/estados",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<StateEntity> create(@RequestBody EstadoDTO estadoDTO){
        if(estadoDTO != null) {
            for (int i = 0; i < estadoDTO.getUf().size(); i++) {
                StateEntity stateEntity = new StateEntity();
                stateEntity.setUf_descricao(estadoDTO.getUf().get(i).getNome());
                stateEntity.setUf_code(estadoDTO.getUf().get(i).getSigla());
                estadoService.save(stateEntity);
            }
            return estadoService.findAll();
        }
        return estadoService.findAll();
    }

    @GetMapping(path = "/estados")
    public List findAll(){
        return estadoService.findAll();
    }

    @GetMapping(path = {"/estados/{id}"})
    public ResponseEntity findById(@PathVariable long id){
        return estadoService.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

}
