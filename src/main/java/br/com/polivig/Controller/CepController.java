package br.com.polivig.Controller;

import br.com.polivig.DTOs.CepDTO;
import br.com.polivig.Service.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/open")
public class CepController {

    @Autowired
    private CepService cepService;

    public CepController(CepService cepService) {
        this.cepService = cepService;
    }

    @GetMapping("/correios/{cep}")
    public CepDTO consultaCNPJ(@PathVariable String cep) {
        return cepService.searchCep(cep);
    }
}
