package br.com.polivig.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "TB_SECONDARY_ACTIVITY")
public class SecondaryActivityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="SECONDARY_ID")
    private long id_atividadePrincipal;

    @Column(name="SECONDARY_CODE", unique = true)
    private String code;

    @Column(name="SECONDARY_TEXT")
    private String text;
}
