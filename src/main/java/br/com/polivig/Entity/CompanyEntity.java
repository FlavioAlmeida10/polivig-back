package br.com.polivig.Entity;

import br.com.polivig.Enum.ETipoEmpresa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "TB_COMPANY")
public class CompanyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COMP_ID")
    private long id_empresa;

    @Column(name = "COMP_STATUS")
    private String status;

    @Column(name = "COMP_END_UPDATE")
    private LocalDate ultima_atualizacao;

    @Column(name = "COMP_CNPJ", unique = true)
    private String cnpj;

    @Column(name = "COMP_TYPE")
    private ETipoEmpresa tipo;

    @Column(name = "COMP_SIZE")
    private String porte;

    @Column(name = "COMP_NAME")
    private String nome;

    @Column(name = "COMP_FANTASY")
    private String fantasia;

    @Column(name = "COMP_OPEN")
    private String abertura;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(name = "COMP_MAIN_ACTIVITY",
            joinColumns = @JoinColumn(name = "COMP_ID"),
            inverseJoinColumns = @JoinColumn(name = "MAIN_ID"))
    private List<MainActivityEntity> atividade_principal;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(name = "COMP_SECONDARY_ACTIVITY",
            joinColumns = @JoinColumn(name = "COMP_ID"),
            inverseJoinColumns = @JoinColumn(name = "SECONDARY_ID"))
    private List<SecondaryActivityEntity> atividades_secundarias;

    @Column(name = "COMP_LEGAL_NATURE")
    private String natureza_juridica;

    @Column(name = "COMP_PBLIC_PLACE")
    private String logradouro;

    @Column(name = "COMP_NUMBER")
    private String numero;

    @Column(name = "COMP_COMPLEMENT")
    private String complemento;

    @Column(name = "COMP_ZIP_CODE")
    private String cep;

    @Column(name = "COMP_NEIGHBORDHOOD")
    private String bairro;

    @Column(name = "COMP_COUNTRY")
    private String municipio;

    @Column(name = "COMP_WOW")
    private String uf;

    @Column(name = "COMP_EMAIL")
    private String email;

    @Column(name = "COMP_PHONE")
    private String telefone;

    @Column(name = "COMP_EFR")
    private String efr;

    @Column(name = "COMP_SITUATION")
    private String situacao;

    @Column(name = "COMP_DATE_SITUATION")
    private String data_situacao;

    @Column(name = "COMP_REASON_SITUATION")
    private String motivo_situacao;

    @Column(name = "COMP_STUATION_SPECIAL")
    private String situacao_especial;

    @Column(name = "COMP_DATESITUATION_ESPECIAL")
    private String data_situacao_especial;

    @Column(name = "COMP_SHARE_CAPITAL")
    private String capital_social;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "COMP_PARTNERS",
            joinColumns = @JoinColumn(name = "COMP_ID"),
            inverseJoinColumns = @JoinColumn(name = "PART_ID"))
    private List<PartnersEntity> qsa;

}
