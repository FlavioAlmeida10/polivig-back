package br.com.polivig.Entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "TB_UF")
public class StateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="UF_ID")
    private long id_uf;

    @Column(name="UF_DECRIPTION")
    private String uf_descricao;

    @Column(name="UF_CODE", unique = true)
    private String uf_code;

}
