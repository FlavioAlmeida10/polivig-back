package br.com.polivig.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "TB_MAIN_ACTIVITY")
public class MainActivityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="MAIN_ID")
    private long id_atividadePrincipal;

    @Column(name="MAIN_CODE", unique = true)
    private String code;

    @Column(name="MAIN_TEXT")
    private String text;
}
