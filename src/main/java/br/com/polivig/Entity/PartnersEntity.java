package br.com.polivig.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "TB_PARTNERS")
public class PartnersEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PART_ID")
    private long id_partners;

    //Nome do Sócio
    @Column(name = "PART_NAME")
    private String nome;

    //Qualificação do Sócio
    @Column(name = "PART_OFFICE")
    private String qual;

    //País de Origem do Sócio
    @Column(name = "PART_COUNTRY_ORIGIN")
    private String pais_origem;

    //Nome do Representante Legal
    @Column(name = "PART_LEGAL_REPRESENTATIVE")
    private String nome_rep_legal;

    //Qualificação do Representante Legal
    @Column(name = "PART_PARTINER_LEGAL_REPRESENTATIVE")
    private String qual_rep_legal;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(	name = "PART_ZIPCODE",
            joinColumns = @JoinColumn(name = "PART_ID"),
            inverseJoinColumns = @JoinColumn(name = "ZIP_ID"))
    private ZipCodeEntity cep;

    @Column(name = "PART_AUTHORIZED")
    private Boolean autorizado;

    @Column(name = "PART_CPF", unique = true)
    private String cpf;

    @Column(name = "PART_RG")
    private String rg;

    @Column(name = "PART_DISPATCHING_AGENCY")
    private String orgExp;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(	name = "PART_STATE_DISPATCHING",
            joinColumns = @JoinColumn(name = "PART_ID"),
            inverseJoinColumns = @JoinColumn(name = "ZIP_ID"))
    private StateEntity ufExp;

    @Column(name = "PART_DISPATCHING_DATE")
    private String dataExp;

    @Column(name = "PART_CTPS")
    private String ctps;

    @Column(name = "PART_SERIE")
    private String serie;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(	name = "PART_CTPS_STATE",
            joinColumns = @JoinColumn(name = "PART_ID"),
            inverseJoinColumns = @JoinColumn(name = "ZIP_ID"))
    private StateEntity ctpsUf;

    @Column(name = "PART_MAIL")
    private String email;

    @Column(name = "PART_PHONE")
    private String telefone;

}
