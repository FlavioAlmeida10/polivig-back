package br.com.polivig.Entity;

import br.com.polivig.Enum.ERole;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@Data
@Table(name = "TB_ROLES")
public class RoleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ROLE_ID")
    private long id_role;

    @Enumerated(EnumType.STRING)
    @Column(name = "ROLE_NAME",length = 20)
    private ERole name;

    public RoleEntity() {
    }
    public RoleEntity(ERole name) {
        this.name = name;
    }
}
