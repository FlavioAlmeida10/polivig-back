package br.com.polivig.Entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "TB_USER",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "NAME_USER"),
                @UniqueConstraint(columnNames = "EMAIL_USER")
        })
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID_USER")
    private long id;

    @NotBlank
    @Size(max = 50)
    @Column(name="NAME_USER")
    private String username;

    @Column(name="EMAIL_USER")
    private String email;

    @Column(name="PHONE_USER")
    private String phone;

    @NotBlank
    @Size(max = 120)
    @Column(name="PASSWORD_USER")
    private String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(	name = "ROLE_USER",
            joinColumns = @JoinColumn(name = "ID_USER"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
    private Set<RoleEntity> roles = new HashSet<>();


    public UserEntity(String username, String email, String phone, String password) {
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.password = password;
    }
}