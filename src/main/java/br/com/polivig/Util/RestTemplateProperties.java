package br.com.polivig.Util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RestTemplateProperties {

    @Value("${resttemplate.readTimeout}")
    private Integer readTimeout;

    @Value("${resttemplate.connectionTimeout}")
    private Integer connectionTimeout;

    @Value("${resttemplate.password}")
    private String password;

    @Value("${resttemplate.protocol}")
    private String protocol;

    public RestTemplateProperties() {
    }

    public Integer getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(Integer readTimeout) {
        this.readTimeout = readTimeout;
    }

    public Integer getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Integer connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public String getPassword() {
        return password = "123456";
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
}
