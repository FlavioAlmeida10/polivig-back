package br.com.polivig.DTOs;

import lombok.Data;

@Data
public class AtividadeSecundariaDTO {
    private String code;
    private String text;
}
