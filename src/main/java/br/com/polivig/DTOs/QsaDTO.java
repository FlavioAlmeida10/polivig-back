package br.com.polivig.DTOs;

import lombok.Data;

@Data
public class QsaDTO {
    //Nome do Sócio
    private String nome;
    //Qualificação do Sócio
    private String qual;
    //País de Origem do Sócio
    private String pais_origem;
    //Nome do Representante Legal
    private String nome_rep_legal;
    //Qualificação do Representante Legal
    private String qual_rep_legal;

    private CepDTO cep;
    private Boolean autorizado;
    private String cpf;
    private String rg;
    private String orgExp;
    private UFDTO ufExp;
    private String dataExp;
    private String ctps;
    private String serie;
    private UFDTO ctpsUf;
    private String email;
    private String telefone;


}
