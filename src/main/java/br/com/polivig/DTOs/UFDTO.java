package br.com.polivig.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UFDTO {

    private long id_uf;
    private String nome;
    private String sigla;

}
