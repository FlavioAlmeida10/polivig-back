package br.com.polivig.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EstadoDTO {
    private List<UFDTO> uf = new ArrayList<UFDTO>();
}
