package br.com.polivig.DTOs;

import br.com.polivig.Enum.ETipoEmpresa;
import lombok.Data;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Data
public class EmpresaDTO {
    private String status;
    private LocalDate ultima_atualizacao;
    private String cnpj;
    private ETipoEmpresa tipo;
    private String porte;
    private String nome;
    private String fantasia;
    private String abertura;
    private List<AtividadePrincipalDTO> atividade_principal;
    private List<AtividadeSecundariaDTO> atividades_secundarias;
    private String natureza_juridica;
    private String logradouro;
    private String numero;
    private String complemento;
    private String cep;
    private String bairro;
    private String municipio;
    private String uf;
    private String email;
    private String telefone;
    private String efr;
    private String situacao;
    private String data_situacao;
    private String motivo_situacao;
    private String situacao_especial;
    private String data_situacao_especial;
    private String capital_social;
    private List<QsaDTO> qsa;
}
