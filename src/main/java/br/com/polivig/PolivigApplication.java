package br.com.polivig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PolivigApplication {

	public static void main(String[] args) {
		SpringApplication.run(PolivigApplication.class, args);
	}

}
