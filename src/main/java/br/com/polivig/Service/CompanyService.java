package br.com.polivig.Service;

import br.com.polivig.DTOs.EmpresaDTO;
import br.com.polivig.Entity.CompanyEntity;
import br.com.polivig.Payload.Response.MessageResponse;
import br.com.polivig.Repository.CompanyRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

    private CompanyRepository companyRepository;
    private ModelMapper mapper = new ModelMapper();
    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public ResponseEntity<CompanyEntity> save(EmpresaDTO empresa){
        CompanyEntity company = new CompanyEntity();
        company = companyRepository.findByCnpj(empresa.getCnpj());
        if (company != null) {
            return ResponseEntity.status(HttpStatus.OK).body(company);
        }
        company = mapper.map(empresa, CompanyEntity.class);
        companyRepository.save(company);
        return ResponseEntity.status(HttpStatus.OK).body(company);
    }

    public EmpresaDTO searchEmpresa(String cnpj){
        EmpresaDTO empresaDTO = new EmpresaDTO();
        CompanyEntity company = companyRepository.findByCnpj(cnpj);
        empresaDTO = mapper.map(company, EmpresaDTO.class);
        return empresaDTO;
    }
}
