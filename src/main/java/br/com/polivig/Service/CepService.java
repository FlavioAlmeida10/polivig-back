package br.com.polivig.Service;

import br.com.polivig.DTOs.CepDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class CepService {

    private final RestTemplate restTemplate;

    @Value("${correios.consulta.cep}")
    private String UrlCorreios;

    public CepService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public CepDTO searchCep(String cep){
        CepDTO searchCep = new CepDTO();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<CepDTO> request = new HttpEntity<>(headers);
        searchCep = restTemplate.exchange(UrlCorreios + cep + "/json/", HttpMethod.GET, request, CepDTO.class).getBody();
        return searchCep;

    }
}
