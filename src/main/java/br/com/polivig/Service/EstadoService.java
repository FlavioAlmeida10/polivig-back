package br.com.polivig.Service;

import br.com.polivig.Entity.StateEntity;
import br.com.polivig.Repository.EstadoRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Service
public class EstadoService implements EstadoRepository {

    private EstadoRepository estadoRepository;

    public EstadoService(EstadoRepository estadoRepository) {
        this.estadoRepository = estadoRepository;
    }


    @Override
    public <S extends StateEntity> S save(S entity) {
        return estadoRepository.save(entity);
    }

    @Override
    public Optional<StateEntity> findById(Long aLong) {
        Optional<StateEntity> estado = Optional.of(new StateEntity());
        estado = estadoRepository.findById(aLong);
        if (estado != null) {
            return estado;
        }
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public List<StateEntity> findAll() {
        return estadoRepository.findAll();
    }

    @Override
    public List<StateEntity> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<StateEntity> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<StateEntity> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public <S extends StateEntity> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends StateEntity> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public <S extends StateEntity> List<S> saveAllAndFlush(Iterable<S> entities) {
        return null;
    }

    @Override
    public void deleteAllInBatch(Iterable<StateEntity> entities) {

    }

    @Override
    public void deleteAllByIdInBatch(Iterable<Long> longs) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public StateEntity getOne(Long aLong) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(StateEntity entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends StateEntity> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public StateEntity getById(Long aLong) {
        return null;
    }

    @Override
    public <S extends StateEntity> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends StateEntity> List<S> findAll(Example<S> example) {

        return null;
    }

    @Override
    public <S extends StateEntity> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends StateEntity> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends StateEntity> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends StateEntity> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends StateEntity, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }
}
