package br.com.polivig.Service;

import br.com.polivig.DTOs.EmpresaDTO;
import br.com.polivig.Entity.CompanyEntity;
import br.com.polivig.Repository.CompanyRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class EmpresaService {

    private final RestTemplate restTemplate;

    @Value("${receita.consulta.cnpj}")
    private String UrlReceita;

    private CompanyRepository companyRepository;

    private ModelMapper mapper = new ModelMapper();

    public EmpresaService(RestTemplate restTemplate, CompanyRepository companyRepository) {
        this.restTemplate = restTemplate;
        this.companyRepository = companyRepository;
    }

    public ResponseEntity<EmpresaDTO> searchEmpresa(String cnpj){
        CompanyEntity company = new CompanyEntity();
        EmpresaDTO empresaDTO = new EmpresaDTO();
        company = companyRepository.findByCnpj(cnpj);
        if (company != null) {
            empresaDTO = mapper.map(company, EmpresaDTO.class);
            return ResponseEntity.status(HttpStatus.OK).body(empresaDTO);
        }else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<EmpresaDTO> request = new HttpEntity<>(headers);
            empresaDTO = restTemplate.exchange(UrlReceita + cnpj, HttpMethod.GET, request, EmpresaDTO.class).getBody();
            return ResponseEntity.status(HttpStatus.OK).body(empresaDTO);
        }
    }
}
