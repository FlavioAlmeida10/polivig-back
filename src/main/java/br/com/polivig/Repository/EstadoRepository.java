package br.com.polivig.Repository;

import br.com.polivig.Entity.StateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoRepository extends JpaRepository<StateEntity, Long> {
}
