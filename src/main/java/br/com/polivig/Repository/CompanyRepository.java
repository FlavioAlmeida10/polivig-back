package br.com.polivig.Repository;

import br.com.polivig.DTOs.EmpresaDTO;
import br.com.polivig.Entity.CompanyEntity;
import br.com.polivig.Entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CompanyRepository extends JpaRepository<CompanyEntity, Long> {
    CompanyEntity findByCnpj(String cnpj);
}
